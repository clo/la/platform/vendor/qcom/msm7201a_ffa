# Copyright (c) 2009, Code Aurora Forum.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_PREBUILT_KERNEL),)

KERNEL_OUT := out/target/product/$(TARGET_PRODUCT)/obj/KERNEL_OBJ
KERNEL_CONFIG := $(KERNEL_OUT)/.config
TARGET_PREBUILT_KERNEL := $(KERNEL_OUT)/arch/arm/boot/zImage

$(KERNEL_OUT):
	mkdir -p $(KERNEL_OUT)

$(KERNEL_CONFIG): $(KERNEL_OUT)
	$(MAKE) -C kernel O=../$(KERNEL_OUT) ARCH=arm CROSS_COMPILE=arm-eabi- msm7201a-perf_defconfig

$(TARGET_PREBUILT_KERNEL): $(KERNEL_OUT) $(KERNEL_CONFIG)
	$(MAKE) -C kernel O=../$(KERNEL_OUT) ARCH=arm CROSS_COMPILE=arm-eabi-

endif

file := $(INSTALLED_KERNEL_TARGET)
ALL_PREBUILT += $(file)
$(file) : $(TARGET_PREBUILT_KERNEL) | $(ACP)
	$(transform-prebuilt-to-target)

file := $(TARGET_OUT_KEYLAYOUT)/7k_ffa_keypad.kl
ALL_PREBUILT += $(file)
$(file) : $(LOCAL_PATH)/7k_ffa_keypad.kl | $(ACP)
	$(transform-prebuilt-to-target)

file := $(TARGET_OUT_KEYLAYOUT)/msm-handset.kl
ALL_PREBUILT += $(file)
$(file) : $(LOCAL_PATH)/msm-handset.kl | $(ACP)
        $(transform-prebuilt-to-target)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := 7k_ffa_keypad.kcm
include $(BUILD_KEY_CHAR_MAP)

# to build the bootloader you need the common boot stuff,
# the architecture specific stuff, and the board specific stuff
include vendor/qcom/$(TARGET_PRODUCT)/boot/Android.mk
# include bootloader/legacy/Android.mk
